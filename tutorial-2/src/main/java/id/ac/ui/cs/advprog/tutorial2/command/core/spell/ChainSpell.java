package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override
    public void cast() {
        int sz = spells.size();
        for(int i = 0; i < sz; i++)
            spells.get(i).cast();
    }

    @Override
    public void undo() {
        int sz = spells.size();
        for(int i = (sz - 1); i >= 0; i--)
            spells.get(i).undo();
    }


    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
