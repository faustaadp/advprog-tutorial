package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getListLog(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return logRepository.findByMahasiswa(mahasiswa);
    }

    @Override
    public Log getLogByIdLog(int idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogByIdLog(int idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public ArrayList<LogSummary> getLogSummary(String npm){
        Iterable<Log> logs = getListLog(npm);
        HashMap<String, Double> total = new HashMap<>();
        ArrayList<LogSummary> ret = new ArrayList<>();

        for (Log log:logs) {
            double durasi = log.getDurasi() / 60.0;
            String month = log.getStart().getMonth().toString();
            if (total.get(month) != null) {
                durasi += total.get(month);
            }
            total.put(month, durasi);
        }
        for (Map.Entry elm:total.entrySet()) {
            String key = elm.getKey().toString();
            LogSummary tmp = new LogSummary(key, total.get(key));
            ret.add(tmp);
        }
        return ret;
    }

    @Override
    public HashMap<String, Double> getTotal(String npm){
        ArrayList<LogSummary> logs = this.getLogSummary(npm);
        double total = 0.0;
        for (LogSummary log:logs) {
            total += log.getPembayaran();
        }
        HashMap<String, Double> ret = new HashMap<>();
        ret.put("totalPayment", total);
        return ret;
    }

}
