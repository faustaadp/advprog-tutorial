package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;

import java.util.ArrayList;
import java.util.HashMap;

public interface LogService {
    Log createLog(String npm, Log log);

    Iterable<Log> getListLog(String npm);

    Log getLogByIdLog(int idLog);

    Log updateLog(int idLog, Log log);

    void deleteLogByIdLog(int idLog);

    ArrayList<LogSummary> getLogSummary(String npm);

    HashMap<String, Double> getTotal(String npm);
}
