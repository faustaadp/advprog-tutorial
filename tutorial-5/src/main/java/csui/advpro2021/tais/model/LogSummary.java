package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

public class LogSummary {
    private String month;
    private double jamKerja;
    private double pembayaran;

    public LogSummary(String month, double jamKerja) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = jamKerja * 350.0;
    }

    public String getMonth(){
        return month;
    }

    public double getJamKerja(){
        return jamKerja;
    }

    public double getPembayaran(){
        return pembayaran;
    }
}
