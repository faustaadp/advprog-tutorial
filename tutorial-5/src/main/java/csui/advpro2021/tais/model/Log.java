package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "idLog", updatable = false)
    private int idLog;

    @Column(name = "start")
    private LocalDateTime start;

    @Column(name = "end")
    private LocalDateTime end;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
    @JoinColumn(name = "npm", nullable = false)
    private Mahasiswa mahasiswa;

    public Log(LocalDateTime start, LocalDateTime end, String deskripsi) {
        this.start = start;
        this.end = end;
        this.deskripsi = deskripsi;
    }
    
    public void setIdLog(int idLog){
        this.idLog = idLog;
    }

    public void setMahasiswa(Mahasiswa mahasiswa){
        this.mahasiswa = mahasiswa;
    }

    public Mahasiswa getMahasiswa(){
        return this.mahasiswa;
    }

    public LocalDateTime getStart(){
        return start;
    }

    public LocalDateTime getEnd(){
        return end;
    }

    public int getDurasi() {
        return (int)Duration.between(start, end).toMinutes();
    }
}
