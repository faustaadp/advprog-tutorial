package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getListLog(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getListLog(npm));
    }

    @PostMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(npm, log));
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity getLog(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(logService.getLogByIdLog(id));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity updateLog(@PathVariable(value = "id") int id, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(id, log));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "id") int id) {
        logService.deleteLogByIdLog(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/{npm}/summary", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getSummary(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getLogSummary(npm));
    }

    @GetMapping(path = "/{npm}/total", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getTotalMoney(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getTotal(npm));
    }

}
