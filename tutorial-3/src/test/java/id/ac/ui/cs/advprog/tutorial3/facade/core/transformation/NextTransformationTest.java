package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NextTransformationTest {
    private Class<?> NextClass;

    @BeforeEach
    public void setup() throws Exception {
        NextClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.NextTransformation");
    }

    @Test
    public void testNextHasEncodeMethod() throws Exception {
        Method translate = NextClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testNextEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Tbgjsb0boe0J0xfou0up0b0cmbdltnjui0up0gpshf0pvs0txpse";

        Spell result = new NextTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testNextEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Tbgjsb0boe0J0xfou0up0b0cmbdltnjui0up0gpshf0pvs0txpse";

        Spell result = new NextTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testNextHasDecodeMethod() throws Exception {
        Method translate = NextClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testNextDecodesCorrectly() throws Exception {
        String text = "Tbgjsb0boe0J0xfou0up0b0cmbdltnjui0up0gpshf0pvs0txpse";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new NextTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testNextDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "Tbgjsb0boe0J0xfou0up0b0cmbdltnjui0up0gpshf0pvs0txpse";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new NextTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}

