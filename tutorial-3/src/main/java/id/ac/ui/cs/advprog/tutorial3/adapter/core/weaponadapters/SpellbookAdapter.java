package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean lastLarge;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.lastLarge = false;
    }

    @Override
    public String normalAttack() {
        lastLarge = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(lastLarge)
           return "Gagal, karena menggunakan large spell 2x beruntun";
        lastLarge = true;
        return spellbook.largeSpell();
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
