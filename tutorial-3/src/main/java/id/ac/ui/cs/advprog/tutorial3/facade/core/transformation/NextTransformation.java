package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class NextTransformation {
    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private int toNum(char ch){
        if('0' <= ch && ch <= '9'){
            return ch - '0';
        }
        else if('A' <= ch && ch <= 'Z'){
            return ch - 'A' + 10;
        } else if('a' <= ch && ch <= 'z'){
            return ch - 'a' + 36;
        } else
            return 62;
    }

    private char toCh(int num){
        if(num < 10){
            return (char)(num + '0');
        }
        else if(num < 36){
            return (char)(num - 10 + 'A');
        } else if(num < 62){
            return (char)(num - 36 + 'a');
        } else
            return ' ';
    }

    private Spell process(Spell spell, boolean encode){
        int tambah = encode ? 1 : -1;
        String text = spell.getText();
        String ret = "";
        Codex codex = spell.getCodex();
        for(int i = 0; i < text.length(); i++){
            ret += toCh((toNum(text.charAt(i)) + tambah + 63) % 63);
        }
        return new Spell(ret, codex);
    }
}
