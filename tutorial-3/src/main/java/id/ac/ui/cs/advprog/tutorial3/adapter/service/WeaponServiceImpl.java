package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private LogRepository logRepository;


    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> weapons = weaponRepository.findAll();
        List<Bow> bows = bowRepository.findAll();
        List<Spellbook> spellbooks = spellbookRepository.findAll();
        for(Bow bow : bows) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                weapons.add(new BowAdapter(bow));
            }
        }
        for(Spellbook spellbook : spellbooks) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                weapons.add(new SpellbookAdapter(spellbook));
            }
        }
        return weapons;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(weapon == null)
        {
            Bow bow = bowRepository.findByAlias(weaponName);
            Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
            if(bow != null)
                weapon = new BowAdapter(bow);
            else
                weapon = new SpellbookAdapter(spellbook);
        }
        if(attackType == 0) {
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() +
                    " (normal attack): " + weapon.normalAttack());
        } else {
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() +
                    " (charged attack): " + weapon.chargedAttack());
        }
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
